Scripts for building an image containing some needed C++ sthuff
=========================================================

Scripts in this repo are [Unlicensed ![](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/).

Third-party components have own licenses.

**DISCLAIMER: BADGES BELOW DO NOT REFLECT THE STATE OF THE DEPENDENCIES IN THE CONTAINER**

This builds a Docker image contains the following components:
  * [`fixed_debian`](https://gitlab.com/KOLANICH-subgroups/docker-images/fixed_debian) docker image
  * clang++, clang, llvm, version 11
  * [`swig`](https://github.com/swig/swig) [![Debian Stable package](https://repology.org/badge/version-for-repo/debian_stable/swig.svg) ![latest packaged version(s)](https://repology.org/badge/latest-versions/swig.svg)](https://repology.org/metapackage/swig/versions) [from Debian packages](https://packages.debian.org/search?keywords=swig) [![GNU General Public License v3](https://www.gnu.org/graphics/gplv3-88x31.png)](./licenses/GPL-3.0.md);
  * dependencies of everything above
