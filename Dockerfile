# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/fixed_debian:latest
LABEL maintainer="KOLANICH"
WORKDIR /tmp
RUN \
  set -ex;\
  eval `apt-config shell TRUSTED_KEYS_DIR Dir::Etc::TrustedParts/d`;\
  echo "TRUSTED_KEYS_DIR $TRUSTED_KEYS_DIR";\
  curl -L -o llvm-official-downloaded.gpg https://apt.llvm.org/llvm-snapshot.gpg.key;\
  sha512sum llvm-official-downloaded.gpg | grep 15a8b6bb63b14a7e64882edbc8a3425b95648624dacd08f965c60cffd69624e69c92193a694dce7f2a3c3ef977d8d1b394b6d9d06b3e10259d25f09d67baea87;\
  gpg --no-default-keyring --keyring ./kr.gpg --import ./llvm-official-downloaded.gpg;\
  gpg --no-default-keyring --keyring ./kr.gpg --export > $TRUSTED_KEYS_DIR/llvm-official.gpg;\
  echo "deb [signed-by=$TRUSTED_KEYS_DIR/llvm-official.gpg] https://apt.llvm.org/unstable/ llvm-toolchain main" > /etc/apt/sources.list.d/llvm-official.list;\
  apt-get update;\
  export LLVM_VERSION=19;\
  export GCC_VERSION=13;\
  apt remove -y "libopencl-clang-*-dev";\
  apt-get install -y gfortran-$GCC_VERSION clang clang++ llvm lld-$LLVM_VERSION libstdc++-$GCC_VERSION-dev;\
  \
  for n in "clang" "clang++" "ld.lld" "llvm-config"; do\
    update-alternatives --install /usr/bin/$n $n /usr/bin/$n-$LLVM_VERSION 100;\
  done;\
  update-alternatives --install /usr/bin/cpp cpp /usr/bin/clang++ 100;\
  update-alternatives --install /usr/bin/c11 c11 /usr/bin/clang 100;\
  update-alternatives --install /usr/bin/c18 c18 /usr/bin/clang 100;\
  update-alternatives --install /usr/bin/c23 c23 /usr/bin/clang 100;\
  update-alternatives --install /usr/bin/cc cc /usr/bin/c18 100;\
  update-alternatives --install /usr/bin/ld ld /usr/bin/ld.lld 100;\
  for n in "nm" "objcopy" "ar" "cov" "rc" "ranlib" "readobj" "tblgen" "cxxdump" "cxxfilt" "strip" "tblgen"; do\
    update-alternatives --install /usr/bin/llvm-$n llvm-$n /usr/bin/llvm-$n-$LLVM_VERSION 100;\
    update-alternatives --install /usr/bin/$n $n /usr/bin/llvm-$n 100;\
  done;\
  apt-get install -y swig pkg-config patchelf;\
  #apt-get remove python3-minimal; # fuck, required for llvm-16-dev llvm-16-tools python3-clang-16 \
  apt-get autoremove --purge -y ;\
  apt-get clean && rm -rf /var/lib/apt/lists/* ;\
  rm -rf /tmp/*
ENV CC=clang CXX=clang++ CCSHARED="clang -shared" CXXSHARED="clang++ -shared" LDSHARED="clang -shared" LDCXXSHARED="clang++ -shared" AR=llvm-ar
#TODO: CFLAGS, SHLIB_SUFFIX, ARFLAGS
